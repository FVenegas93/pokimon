package pomekon;



public class Pokemon {
	protected String nombre;
	protected int nivel;
	protected int hp;
	protected int atk;
	protected int def;
	protected int spa;
	protected int spd;
	protected int spe;
	protected Tipo tipo1;
	protected Tipo tipo2;
	protected Movimientos movimiento1;
	protected Movimientos movimiento2;
	protected Movimientos movimiento3;
	protected Movimientos movimiento4;
	
	public Pokemon(String nombre, int nivel, int hp, int atk, int def, int spa, int spd, int spe, Tipo tipo1, Tipo tipo2,
			Movimientos movimiento1, Movimientos movimiento2, Movimientos movimiento3, Movimientos movimiento4) {
		this.nombre=nombre;
		this.nivel=nivel;
		this.hp=hp;
		this.atk=atk;
		this.def=def;
		this.spa=spa;
		this.spd=spd;
		this.spe=spe;
		this.tipo1=tipo1;
		this.tipo2=tipo2;
		this.movimiento1=new Movimientos();
		this.movimiento2=new Movimientos();
		this.movimiento3=new Movimientos();
		this.movimiento4=new Movimientos();
	}

	public Pokemon() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getSpa() {
		return spa;
	}

	public void setSpa(int spa) {
		this.spa = spa;
	}

	public int getSpd() {
		return spd;
	}

	public void setSpd(int spd) {
		this.spd = spd;
	}

	public int getSpe() {
		return spe;
	}

	public void setSpe(int spe) {
		this.spe = spe;
	}

	public Tipo getTipo1() {
		return tipo1;
	}

	public void setTipo1(Tipo tipo1) {
		this.tipo1 = tipo1;
	}

	public Tipo getTipo2() {
		return tipo2;
	}

	public void setTipo2(Tipo tipo2) {
		this.tipo2 = tipo2;
	}
	
}
