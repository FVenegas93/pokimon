package pomekon;

import java.util.Random;


public class Movimientos {
	protected String nombreMov;
	protected int potencia;
	protected Tipo tipoMov;
	protected double stab;
	protected double efectividad;
	protected double dmg;
	
	public Movimientos(String nombreMov, int potencia, Tipo tipoMov) {
		this.nombreMov = nombreMov;
		this.potencia = potencia;
		this.tipoMov = tipoMov;
	}

	public Movimientos() {
		// TODO Auto-generated constructor stub
	}

	public String getNombreMov() {
		return nombreMov;
	}

	public void setNombreMov(String nombreMov) {
		this.nombreMov = nombreMov;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public Tipo getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(Tipo tipoMov) {
		this.tipoMov = tipoMov;
	}
	
	public double calcularSTAB(Pokemon p) {
		
		if (tipoMov.getNombreTipo()==p.getTipo1().getNombreTipo()) {
			stab=1.5;
		}else
			stab=1;
		return stab;
	}
	
	public void calcularEfectividad(Pokemon p) {
		
		
		switch(tipoMov) {
		case PLANTA:
			if (p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2==null) {
				efectividad=2;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2==null) {
				efectividad=0.5;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("normal") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2==null) {
				efectividad=0.5;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2.getNombreTipo().equalsIgnoreCase("ps�quico")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2.getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=2;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2.getNombreTipo().equalsIgnoreCase("veneno")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2.getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.25;
			}
		break;
		case AGUA:
			if (p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2==null) {
				efectividad=0.5;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("normal") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2==null) {
				efectividad=0.5;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2.getNombreTipo().equalsIgnoreCase("ps�quico")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2.getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2.getNombreTipo().equalsIgnoreCase("veneno")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2.getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=0.5;
			}
		break;
		case NORMAL:
			if (p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("normal") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2==null) {
				efectividad=1;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2.getNombreTipo().equalsIgnoreCase("ps�quico")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2.getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2.getNombreTipo().equalsIgnoreCase("veneno")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2.getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1;
			}
		break;
		case VENENO:
			if (p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2==null) {
				efectividad=0.5;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("normal") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2==null) {
				efectividad=2;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2.getNombreTipo().equalsIgnoreCase("ps�quico")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2.getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=1;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2.getNombreTipo().equalsIgnoreCase("veneno")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2.getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=1;
			}
		break;
		case PSIQUICO: 
			if (p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2==null) {
				efectividad=2;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("normal") && p.tipo2==null) {
				efectividad=1;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2==null) {
				efectividad=0.5;
			}else if (p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2==null) {
				efectividad=1;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("agua") && p.tipo2.getNombreTipo().equalsIgnoreCase("ps�quico")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("ps�quico") && p.tipo2.getNombreTipo().equalsIgnoreCase("agua"))) {
				efectividad=0.5;
			}else if ((p.tipo1.getNombreTipo().equalsIgnoreCase("planta") && p.tipo2.getNombreTipo().equalsIgnoreCase("veneno")) || (p.tipo1.getNombreTipo().equalsIgnoreCase("veneno") && p.tipo2.getNombreTipo().equalsIgnoreCase("planta"))) {
				efectividad=2;
			}
		break;

			
		}
	}
	
	public void calcularDamage(Pokemon p1, Pokemon p2, Movimientos m) {
		Random rng = new Random();
		int variacion = rng.nextInt(100-85+1) + 85;
		dmg = Math.round(0.01 * stab * efectividad * variacion * (((0.2 * p1.getNivel() + 1) * p1.getAtk() * m.getPotencia()) / 25 * p2.getDef()) + 2);
	}
}
