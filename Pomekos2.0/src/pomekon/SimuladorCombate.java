package pomekon;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



public class SimuladorCombate extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	Movimientos placaje = new Movimientos("PLACAJE", 30, Tipo.NORMAL);
	Movimientos bombalodo = new Movimientos("BOMBA LODO", 90, Tipo.VENENO);
	Movimientos hojaafilada = new Movimientos ("HOJA AFILADA", 110, Tipo.PLANTA);
	Movimientos megaagotar = new Movimientos ("MEGA AGOTAR", 40, Tipo.PLANTA);
	Movimientos psiquico = new Movimientos ("PS�QUICO", 95, Tipo.PSIQUICO);
	Movimientos rayoburbuja = new Movimientos ("RAYO BURBUJA", 65, Tipo.AGUA);
	Movimientos golpecuerpo = new Movimientos ("GOLPE CUERPO", 85, Tipo.NORMAL);
	
	Pokemon bulbasaur = new Pokemon("BULBASAUR", 60, 140, 130, 130, 200, 150, 134, Tipo.PLANTA, Tipo.VENENO, 
			placaje, bombalodo, hojaafilada, megaagotar);
	
	Pokemon poliwhirl = new Pokemon("POLIWHIRL", 56, 120, 130, 170, 186, 180, 200, Tipo.AGUA, null,
			placaje, rayoburbuja, psiquico, golpecuerpo);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SimuladorCombate frame = new SimuladorCombate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SimuladorCombate() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10, 10, 600, 1000);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel lb1 = new JLabel("Tu Pok�mon");
		lb1.setBounds(30, 30, 100, 30);
		contentPane.add(lb1);
		
		JLabel lb2 = new JLabel("Pok�mon Rival");
		lb2.setBounds(160, 30, 100, 30);
		contentPane.add(lb2);
		
		JComboBox<String> cb1 = new JComboBox<String>();
		cb1.addItem(bulbasaur.getNombre());
		cb1.addItem(poliwhirl.getNombre());
		cb1.setBounds(30, 80, 100, 30);
		contentPane.add(cb1);
		
		JComboBox<String> cb2 = new JComboBox<String>();
		cb2.addItem(bulbasaur.getNombre());
		cb2.addItem(poliwhirl.getNombre());
		cb2.setBounds(160, 80, 100, 30);
		contentPane.add(cb2);
		
		JButton aceptar = new JButton("ACEPTAR");
		aceptar.setBounds(30, 130, 100, 30);
		contentPane.add(aceptar);
		
		aceptar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//setState(ICONIFIED);
				String n1 = cb1.getSelectedItem().toString();
				String n2 = cb2.getSelectedItem().toString();
				
				JLabel nom_Aliado = new JLabel(n1);
				nom_Aliado.setBounds(30, 350, 100, 30);
				contentPane.add(nom_Aliado);
				
				JLabel nom_Rival = new JLabel(n2);
				nom_Rival.setBounds(400, 130, 100, 30);
				contentPane.add(nom_Rival);
				
				switch(n1) {
				case "BULBASAUR": 
					JLabel bulbasaur_Aliado = new JLabel(new ImageIcon("src\\back_sprite_bulbasaur.png"));
					bulbasaur_Aliado.setBounds(30, 400, 150, 150);
					contentPane.add(bulbasaur_Aliado);
				break;
				case "POLIWHIRL": 
					JLabel poliwhirl_Aliado = new JLabel(new ImageIcon("src\\back_sprite_poliwhirl.png"));
					poliwhirl_Aliado.setBounds(30, 400, 150, 150);
					contentPane.add(poliwhirl_Aliado);
				break;
				}
				
				switch(n2) {
				case "BULBASAUR":
					JLabel bulbasaur_Rival = new JLabel(new ImageIcon("src\\front_sprite_bulbasaur.png"));
					bulbasaur_Rival.setBounds(400, 180, 150, 150);
					contentPane.add(bulbasaur_Rival);
				break;
				case "POLIWHIRL":
					JLabel poliwhirl_Rival = new JLabel(new ImageIcon("src\\front_sprite_poliwhirl.png"));
					poliwhirl_Rival.setBounds(400, 180, 150, 150);
					contentPane.add(poliwhirl_Rival);
				}
			}
			
		});
	}

}
